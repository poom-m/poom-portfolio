

// fixed main-nav
var $mn = $(".main-nav");
var sc = "-scrolled";

$(window).scroll(function() {
	if($(this).scrollTop() > $("header").height()) {
		$mn.addClass(sc);
	} else {
		$mn.removeClass(sc);
	}
});

// trigger mobile menu
var $mna = $(".menubar > a")
var $mnul = $(".main-nav > ul")

$(window).resize(function () {
	if($( window ).width() > 660) {
		$mnul.css("display","block");
	} else {
		$mnul.css("display","none");
	}
});

$mna.click(function hideMenu() {
	if($mnul.css("display") !== "block") {
		$mnul.css("display","block");
	} else {
		$mnul.css("display","none");
	}
});

// main navigation behavior
var $navLink = $(".main-nav > ul li a");
$navLink.click(function(event) {
	event.preventDefault();

	if($( window ).width() <= 660) {
		$mnul.css("display","none");
	}

	var loc = $(this).attr('href');
	$(document).scrollTop( $(loc).offset().top - 59);
});